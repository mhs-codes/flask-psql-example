# -*- coding: utf-8 -*-
import os

def cfg(env, default):
    try:
        return os.environ[env] 
    except:
        return default


DEBUG = cfg('DEBUG', False)
PRODUCTION = cfg('PRODUCTION', 'NO')
STAGING = cfg('STAGING', 'NO')
FORCE_HTTPS = cfg('FORCE_HTTPS', 'NEVER')
SECRET_KEY = cfg('SECRET_KEY', '9238742j3h4gj2h3g423') # make sure to change this
# validity period for oneclick share tokens in seconds
ONECLICK_TOKEN_VALIDITY = cfg('ONECLICK_TOKEN_VALIDITY', (86400 * 5))
PASSWORD_SENTINEL = 'AkjhSkjh76D'
PASSWORD_EXPIRATION_DAYS = 90

# don;t use SERVER_NAME due to flask side effects use SOMEGA_SERVER_NAME
SOMEGA_SERVER_NAME = cfg('SOMEGA_SERVER_NAME', 'localhost:5000')
SOMEGA_SCHEME = 'https' if FORCE_HTTPS == 'ALWAYS' else 'http'
BASE_URL = SOMEGA_SCHEME + '://' + SOMEGA_SERVER_NAME

# prefix for shortened urls
SHURL_PREFIX = cfg('SHURL_PREFIX', BASE_URL + '/u/') 

# DB for URL shortening and stats
SHURL_DB_URL = cfg('SHURL_DB_URL', 'mongodb://localhost:27017/shurls')

# DB name for URL shortening and stats
SHURL_DB_NAME = cfg('SHURL_DB_NAME', 'shurls')

# number of chars in for shortened URL
SHURL_NUM_CHARS = cfg('SHURL_NUM_CHARS', 6)

# if using REST API to add URLs for shortening
SHURL_SECRET = cfg('SHURL_SECRET', 'sdkjfksjdfhksdjh')

# use mongo as celery backend. same db as SHURL_DB_URL
BROKER_URL = cfg('SHURL_DB_URL', 'mongodb://localhost:27017/shurls')
#CELERY_RESULT_BACKEND = cfg('SHURL_DB_URL', 'mongodb://localhost:27017/shurls')

# main DB URL
SQLALCHEMY_DATABASE_URI = cfg('DATABASE_URL', 
                              'postgres://postgres:postgres@localhost/flask01')


# notification emails will be sent to these email addresses

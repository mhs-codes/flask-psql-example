from somega.core import app, login_manager
from flask_security import UserMixin, RoleMixin

from somega.core import db

DEFAULT_VIEW_POINTS = 20
DEFAULT_SHARE_POINTS = 50



class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean())
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(1024))
    value = db.Column(db.String(1024))
    create_user = db.Column(db.Integer(), db.ForeignKey('user.id'))

    def __init__(self, key, value):
        self.key = key
        self.value = value

    def __repr__(self):
        return f'{self.key}{self.value}'


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


with app.app_context():
    db.create_all()

import requests

import werkzeug
from flask import render_template, url_for, redirect, send_from_directory, jsonify
from flask import request
# noinspection PyUnresolvedReferences
from flask_login import current_user, login_user, login_required, logout_user
# noinspection PyUnresolvedReferences
from flask_security import login_required
from flask_security.confirmable import generate_confirmation_link
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired

from somega.models import User
from somega.core import login_manager

from somega import app

# routing for API endpoints (generated from the models designated as API_MODELS)

# routing for CRUD-style endpoints
# passes routing onto the angular frontend if the requested resource exists
from sqlalchemy.sql import exists

# crud_url_models = app.config['CRUD_URL_MODELS']


@app.route('/login', methods=['GET'])
def view_login():
    return render_template('login.html')


@app.route('/login/submit', methods=['POST'])
def do_login(**kwargs):
    data = request.form
    user = User.query.get(1)
    res = login_user(user)
    return 'logged in', 200

@app.route('/logout', methods=['GET'])
def do_logout():
    logout_user()
    return werkzeug.redirect('/login')


@app.route('/', methods=['GET'])
def view_home(**kwargs):
    if getattr(current_user, 'email', False):
        return render_template('index.html')
    return str(current_user), 200


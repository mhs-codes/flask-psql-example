from somega.models import UsersTeams

def is_authorized_to_read(teamid, userid):
    try:
        ut = UsersTeams.query.filter(
            UsersTeams.user_id==userid).filter(
            UsersTeams.team_id==teamid).first()
        if ut:
            return True
        else:
            return False
    except:
        return False

def is_authorized_to_write(teamid, userid):
    try:
        ut = UsersTeams.query.filter(
            UsersTeams.user_id==userid).filter(
            UsersTeams.team_id==teamid).first()
        return ut.is_admin
    except:
        return False


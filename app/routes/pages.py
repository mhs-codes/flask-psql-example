from flask import render_template, session, redirect
# noinspection PyUnresolvedReferences
from flask_security import login_required, current_user

from somega.core import app

@app.route('/register-success', methods=['GET'])
def register_success():
    return render_template('register-success.html')


@app.route('/admin-videos', methods=['GET'])
@login_required
def show_admin_video_page():
    return render_template('admin-videos.html')


@app.route('/user-videos', methods=['GET'])
@login_required
def show_user_videos():
    return render_template('user-videos.html')


@app.route('/welcome-video-user', methods=['GET'])
@login_required
def show_user_video_page():
    return render_template('welcome-video-user.html')


@app.route('/welcome-video', methods=['GET'])
@login_required
def welcome_video():
    teams = get_user_teams(current_user.id)
    nextLink = '/create-team'
    if teams:
        connections = get_user_connections(current_user.id)
        if not connections:
            nextLink = '/connect-social-accounts'
        else:
            nextLink = '/invite-users'
    return render_template('welcome-video.html', nextLink=nextLink)


def clear_social_session():
    if 'SOCIAL_PAGE' in session:
        del session['SOCIAL_PAGE']
    pass


def check_social_session():
    return 'SOCIAL_PAGE' in session and session['SOCIAL_PAGE']


@app.route('/create-team', methods=['GET'])
@login_required
def create_team():
    return render_template('onboard.html', hide_cancel=True)


@app.route('/connect-social-accounts', methods=['GET'])
@login_required
def connect_social_accounts():
    session['SOCIAL_PAGE'] = True
    teams = get_user_teams(current_user.id)
    next_link = '/user-final'
    if len(teams) == 1 and teams[0].is_admin:
        next_link = '/admin-invite'
    return render_template('connect-social-accounts.html', next_link=next_link)


@app.route('/admin-invite', methods=['GET'])
@login_required
def admin_invite_page():
    teams = get_user_teams(current_user.id)
    if len(teams) == 1 and teams[0].is_admin:
        return render_template('admin-invite.html', teamid=teams[0].team_id)
    return redirect('/admin-final')


@app.route('/admin-final', methods=['GET'])
@login_required
def admin_final_page():
    clear_social_session()
    return render_template('/admin-final.html')


@app.route('/user-final', methods=['GET'])
@login_required
def user_final_page():
    clear_social_session()
    return render_template('/user-final.html')

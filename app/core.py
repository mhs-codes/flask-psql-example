from threading import Thread
from flask import Flask, request, redirect
# noinspection PyUnresolvedReferences
from flask_sqlalchemy import SQLAlchemy
# noinspection PyUnresolvedReferences
from flask_security import Security, SQLAlchemyUserDatastore

# noinspection PyUnresolvedReferences
# from flask_login import current_user
from flask_login import user_logged_in, LoginManager

# noinspection PyUnresolvedReferences
# from flask.ext.security.signals import password_changed, password_reset
# noinspection PyUnresolvedReferences


app = Flask(__name__)
app.config.from_object('somega.localsettings')
app.url_map.strict_slashes = False

from .localsettings import *

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

db = SQLAlchemy(app)
from somega.models import *
